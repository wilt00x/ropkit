# -*- coding: utf-8 -*-

"""

params_module.py : Module

Version: 1.0.0
Author: wil.tor	
License: https://opensource.org/licenses/GPL-3.0

"""

from core import *

def params_module(aslr, nx, pie):
	s_aslr           = ""
	s_nx             = ""
	s_pie            = ""

	if aslr is True: s_aslr = G+"ASLR enabled "+W
	else: s_aslr = R+"ASLR disabled"+W
	if nx is True: s_nx = G+"NX enabled "+W
	else: s_nx = R+"NX disabled"+W
	if pie is True: s_pie = G+"PIE enabled "+W
	else: s_pie = R+"PIE disabled"+W 

	print(W+"\n+-------------+---------------------+")
	print("| Protections | Enabled or disabled |")
	print("+-------------+---------------------+")
	print("|===> ASLR    |     {}   |".format(s_aslr))
	print("+-------------+---------------------+")
	print("|===> NX      |     {}     |".format(s_nx))
	print("+-------------+---------------------+")
	print("|===> PIE     |     {}    |".format(s_pie))
	print("+-------------+---------------------+")
	print(O+"|==> Activate or Desactivate protection with 'active' or 'desactive'\n"+W)


